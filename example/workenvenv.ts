import { randomBytes } from 'crypto';

import { Work } from '../work';
import { Wallet } from '../wallet';
import { Envelope } from '../envelope';
import { Contact, KeyContact } from '../contact';
import { Message } from '../message';
import { zeroHash } from '../types';
let process = require('process');

let aliceMnemonic = "soup high fat equip protect unusual tag helmet detail erupt hover boss glad crew relief narrow message grid swing tone physical inquiry satisfy habit";
let bobMnemonic = "indicate acid noodle option control winner match crouch suit soda increase unknown monitor produce resource crane correct outdoor viable frozen leave funny cement again";

// simulated hash representing alice's last signature on some data
let aliceLastHash = randomBytes(32);


// simulated hash representing bob's last signature on some data
let bobLastHash = randomBytes(32);


// alice's wallet
let wallet = new Wallet(aliceMnemonic);


// alice's work
let contactAlice = new Contact('alice', undefined);
let now = Math.floor(Date.now()/1000);
let work = new Work('what a dull boy', 42, now);


// envelope holding alice's signature on her work
let envelopeAlice = new Envelope(aliceLastHash, work);
envelopeAlice.sign(wallet);


// alice wraps the envelope and contact in a message - this is the vehicle for cross-device transfer
let messageAlice = new Message(envelopeAlice, contactAlice);
let messageAliceJson = JSON.stringify(messageAlice);


//
// ... let's pretend alice sends json data to bob ...
//


// bob's wallet
let walletBob = new Wallet(bobMnemonic);


// bob unwraps alice's envelope and verifies her message 
let messageAliceRecovered = Message.fromJSON(messageAliceJson);
let verifyAlice = messageAliceRecovered.verify() 


// bob can also unpack her details
let contactAliceRecovered = messageAliceRecovered.getContact();


// bob unpack alice's work
let workSerialRecovered = messageAliceRecovered.getContent(); //envelopeAliceRecovered.content;
let workRecovered = Work.deserialize(workSerialRecovered);


// then bob signs alice's signature envelope
let contactBob = new Contact('bob', undefined);
let envelopeBob = new Envelope(bobLastHash, envelopeAlice);
envelopeBob.sign(walletBob);


// bob wraps the envelope and his contact in a message
let messageBob = new Message(envelopeBob, contactBob);
let messageBobJson = JSON.stringify(messageBob);


//
// ... let's pretend again, this time that bob sends json data to alice ...
//


// alice can unpack bob's signature and recover his public key
let messageBobRecovered = Message.fromJSON(messageBobJson);
let verifyBob = messageBobRecovered.verify();

//let envelopeBobRecovered = messageBobRecovered.envelope;
let contactBobRecovered = messageBobRecovered.getContact(); //KeyContact.fromContact(messageBobRecovered.contact, publicKeyBobRecovered);


// strut
console.log(contactAliceRecovered.commonName + '\'s public key:', contactAliceRecovered.publicKey, verifyAlice);
console.log(contactBobRecovered.commonName + '\'s public key:', contactBobRecovered.publicKey, verifyBob);
console.log(contactAliceRecovered.commonName + '\'s work:', workRecovered.description, workRecovered.weight, workRecovered.timestamp);
